var request = require('supertest');
request = request('https://swapi.dev/api/');

const chai = require('chai');

describe('Films', () => {
    
    it('GET films/:id ', async () => {
        const filmRes = await request.get('films/2/').expect(200);
        filmRes.body.should.have.property('title')
          .to.be.eql('The Empire Strikes Back');
        });
});
