var request = require('supertest');
request = request('https://swapi.dev/api/');

const chai = require('chai');
const should = chai.should();

const swapi = require('swapi-node');

describe('People', () => {
    
    it('GET people/:id ', async () => {
        const swResult = await swapi.getPerson(2);
        const swName = await swResult.getName();
        const peopleRes = await request.get('people/2/').expect(200);
        peopleRes.body.should.have.property('name').to.be.eql(swName);
        });
});

describe('Films', () => {
    
    it('GET films/:id ', async () => {
        const swResult = await swapi.getFilm(2);
        const swTitle = await swResult.getTitle();
        const filmRes = await request.get('films/2/').expect(200);
        filmRes.body.should.have.property('title').to.be.eql(swTitle);
        });
});
